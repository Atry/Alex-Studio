//#include <stdlib.h>
#include <argp.h>

const char * argp_program_version = "generateur_playlist v0.1";
const char * argp_program_bug_address = "<alexandre.marques@bts-malraux.net>";

/* documentation du programme  */
static char * doc = "commencement de la CLI";

/*analyseur argp */
static struct argp argp = {option, parse_opt, args_doc, doc};

int main (int nombre_arguments, char **tab_arguments)
{
    argp_parse(&argp, nombre_arguments, tab_arguments, 0, 0, 0);
    exit(0);
}
