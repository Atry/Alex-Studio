#include <iostream>
#include <stdio.h>
#include <postgresql/libpq-fe.h>
#include <string>
#include <syslog.h>

int     main() {
PGconn          *conn;
PGresult        *res;
//int             rec_count;
//int             row;
//int             col;
int             i = 0;

openlog("principal", LOG_CONS | LOG_PID, LOG_USER);
syslog(LOG_DEBUG, "Programme lancé");
closelog();

        conn = PQconnectdb("dbname=radio_libre host=172.16.99.2 port=5432 user=a.atry password=P@ssword");

        if (PQstatus(conn) == CONNECTION_BAD) {
               std::cout << "Connection refusé" << std::endl;
        }

	std::cout << "Connection réussie" << std::endl;

	res = PQexec(conn, "select count(*) from morceaux");

            if (PQresultStatus(res) != PGRES_TUPLES_OK)         /* did the query fail? */
            {
                fprintf(stderr, "SELECT query failed.\n");
                PQclear(res);
                PQfinish(conn);
                exit(1);
            }
         
            for (i = 0; i < PQntuples(res); i++)                /* loop through all rows returned */
                
		std::cout << PQgetvalue(res, i, 0) << std::endl;	/* print the value returned */

            PQclear(res);                                       /* free result */
         
            PQfinish(conn);                                     /* disconnect from the database */
         
            

        return 0;
}
